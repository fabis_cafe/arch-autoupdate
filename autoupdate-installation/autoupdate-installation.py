#!/usr/bin/env python3
## Script to install updates on shutdown
## also tries to skip this under some circumstances

import configparser
import fnmatch
import io
import os
import shutil
import subprocess
import sys

def funct_error_exit(error_message):
    print(error_message, file=sys.stderr)
    sys.exit(1)
def funct_normal_exit(normal_message):
    print(normal_message, file=sys.stdout)
    sys.exit(0)

# Set LANG to C, so that command output is more predictable
os.environ['LANG'] = "C"

# Streamline config naming
AUTOUPDATE_CONF_PATH = '/usr/share/arch-autoupdate/autoupdate.conf'

# Read config
if not os.path.isfile(AUTOUPDATE_CONF_PATH):
    funct_error("Config file not found")

## allow_no_value=True <- to allow keys without value, like ILoveCandy
## strict=False <- do not run into errors, when duplicates are in the file
autoupdate_config = configparser.ConfigParser(allow_no_value=True, strict=False)
autoupdate_config.read(AUTOUPDATE_CONF_PATH)
## Set the vars we need in here
UPDATER_CACHE_PATH = autoupdate_config.get('options', 'CacheDir',
                                           fallback='/var/cache/autoupdate/pkg')

## Check pathes for vars
if not os.path.isdir(UPDATER_CACHE_PATH):
    funct_error_exit(UPDATER_CACHE_PATH + " not found.")

## If there are updates, but no repository-db-file. The update loader couldn't 
## finish its job, so nothing to do here.
if not os.path.isfile(os.path.join(UPDATER_CACHE_PATH,"autoupdate.db.tar.gz")):
    sys.exit(0)

# Check for battery status
## It's better to not run the updater if the battery is low
## Since there is no status for all batteries in the system, I check
## them one by one, write everything into a BAT_CAP and have the total sum.
## if it is >= 35%, lets do the update, otherwise skip
BAT_DIRNAMES = fnmatch.filter(os.listdir('/sys/class/power_supply'), 'BAT*')
BAT_COUNT = len(BAT_DIRNAMES)
## if there is no battery, skip this part
if BAT_COUNT > 0:
    BAT_CAP = 0
    for BAT_LOOPN in range(0, BAT_COUNT):
        BAT_PATH = os.path.join('/sys/class/power_supply',
                    BAT_DIRNAMES[BAT_LOOPN], 'capacity')
        BAT_CAP_READ = open(BAT_PATH, "r")
        BAT_CAP = BAT_CAP + int(BAT_CAP_READ.read().strip())
        BAT_CAP_READ.close()

    if BAT_CAP <= 35:
        funct_normal_exit("battery low, skip update")

# Cache dir of normal pacman
## I need to get the normal cache dir, to move files to.
## I could hardcode this, but thats a bit to easy. So look for the pacman.conf
## and use this cache dir if set.
if not os.path.isfile("/etc/pacman.conf"):
    funct_error_exit("/etc/pacman.conf not found.")

## allow_no_value=True <- to allow keys without value, like ILoveCandy
## strict=False <- do not run into errors, when duclicates are in the file
pacman_config = configparser.ConfigParser(allow_no_value=True, strict=False)
pacman_config.read("/etc/pacman.conf")
PACMAN_CACHE_PATH = pacman_config.get('options', 'CacheDir', fallback='/var/cache/pacman/pkg')

if not os.path.isdir(PACMAN_CACHE_PATH):
    funct_error_exit("Invalid pacman cache path: " + PACMAN_CACHE_PATH)

# Call pacman
## Upgrade everything
args_app_pacman = ["/usr/bin/pacman", "--config", AUTOUPDATE_CONF_PATH,
                    "--sync", "--refresh", "--sysupgrade", "--needed",
                    "--noprogressbar", "--noconfirm", "--verbose",
                    "--overwrite", "/usr/share/*",
                    "--overwrite", "/usr/lib/*",
                    "--overwrite", "/usr/lib32/*"]

app_pacman = subprocess.run(args_app_pacman,
                            capture_output=True,
                            universal_newlines=True)

print(app_pacman.stdout, file=sys.stdout)

if app_pacman.returncode != 0:
    print(app_pacman.stdout, file=sys.stdout)
    print(app_pacman.stderr, file=sys.stderr)
    sys.exit(1)

## move packages
for mv_filename in os.listdir(UPDATER_CACHE_PATH):
    if fnmatch.fnmatch(mv_filename, '*.pkg.*'):
        shutil.move(os.path.join(UPDATER_CACHE_PATH ,mv_filename),
                    os.path.join(PACMAN_CACHE_PATH ,mv_filename))

# Check for repo files and remove them
REPO_DB_FILES = ["autoupdate.db", "autoupdate.db.tar.gz", "autoupdate.files",
                "autoupdate.files.tar.gz", "autoupdate.db.tar.gz.old",
                "autoupdate.files.tar.gz.old"]
REPO_DB_VALUE = 0
while REPO_DB_VALUE < len(REPO_DB_FILES):
    if os.path.isfile(os.path.join(UPDATER_CACHE_PATH, REPO_DB_FILES[REPO_DB_VALUE])):
        os.remove(os.path.join(UPDATER_CACHE_PATH, REPO_DB_FILES[REPO_DB_VALUE]))
    REPO_DB_VALUE = REPO_DB_VALUE + 1

sys.exit(0)
